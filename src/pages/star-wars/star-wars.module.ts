import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StarWarsPage } from './star-wars';

@NgModule({
  declarations: [
    StarWarsPage,
  ],
  imports: [
    IonicPageModule.forChild(StarWarsPage),
  ],
})
export class StarWarsPageModule {}
