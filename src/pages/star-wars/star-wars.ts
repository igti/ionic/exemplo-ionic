import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Jedi } from './Jedi';

/**
 * Generated class for the StarWarsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-star-wars',
  templateUrl: 'star-wars.html',
})
export class StarWarsPage {

  jedis: Jedi[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.jedis.push(new Jedi('Luke Skywalker', 1.9));
    this.jedis.push(new Jedi('Darth Vader', 2.3));
    this.jedis.push(new Jedi('Yoda', 4.9));
    this.jedis.push(new Jedi('Obi Wan Kenobi', 1.7));
    this.jedis.push(new Jedi('Rey', 1.7));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StarWarsPage');
  }

  cadastrarJedi(nome: string) {
    this.jedis.push(new Jedi(nome));
  }
}
