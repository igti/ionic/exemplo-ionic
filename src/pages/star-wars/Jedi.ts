export class Jedi {

  private _nome: string;
  private _forca: number;
  private _dataCriacao: Date;

  constructor(pNome: string, pFatorForca: number = 1.5) {
    this._nome = pNome;
    this._forca = this._nome.length * pFatorForca;
    this._dataCriacao = new Date();
  }

  get nome() {
    return this._nome;
  }

  get forca() {
    return this._forca;
  }

  get dataCriacao() {
    return this._dataCriacao;
  }
}
